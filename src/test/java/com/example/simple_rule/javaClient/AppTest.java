package com.example.simple_rule.javaClient;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.drools.core.command.runtime.rule.InsertObjectCommand;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.internal.runtime.helper.BatchExecutionHelper;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieServiceResponse.ResponseType;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

import com.example.simple_rule.ContinentMap;
import com.example.simple_rule.Country;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public AppTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(AppTest.class);
  }

  /**
   * Rigorous Test :-)
   */
  public void testApp() {

    String url = "http://localhost:8080/kie-server/services/rest/server";
    String username = "kieServerUser";
    String password = "kieServerUser1234;";
    String container = "geo_location";
    String session = "restLess";

    KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(url, username, password);

    config.setMarshallingFormat(MarshallingFormat.JSON);

    KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

    RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
    List<Command<?>> commands = new ArrayList<Command<?>>();

    KieCommands kieCommander = KieServices.Factory.get().getCommands();

    {
      ContinentMap fact = new ContinentMap("Europe", "United Kingdom");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Europe", "France");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Europe", "Italy");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Europe", "Germany");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Europe", "Greece");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Asia", "India");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Asia", "Japan");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Asia", "Thailand");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Asia", "Indonesia");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      ContinentMap fact = new ContinentMap("Asia", "Maldives");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      Country fact = new Country("France");
      commands.add(new InsertObjectCommand(fact));
    }
    {
      Country fact = new Country("Maldives");
      commands.add(new InsertObjectCommand(fact));
    }

    commands.add(new FireAllRulesCommand("fireAll"));

    BatchExecutionCommand batchCommand = kieCommander.newBatchExecution(commands, session);
    {
      String bbXML = BatchExecutionHelper.newXStreamMarshaller().toXML(batchCommand);
      String bbJSON = BatchExecutionHelper.newJSonMarshaller().toXML(batchCommand);
      try {
        FileWriter fw = new FileWriter(new File(("batchCommand.xml")));
        fw.write(bbXML);
        fw.close();
        System.out.println("..::|| === batchCommand has been saved as XML to batchCommand.xml");
      } catch (IOException e) {
        e.printStackTrace();
      }
      try {
        FileWriter fw = new FileWriter(new File(("batchCommand.json")));
        fw.write(bbJSON);
        fw.close();
        System.out.println("..::|| === batchCommand has been saved as JSON to batchCommand.json");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(container, batchCommand);

    if (response.getType().compareTo(ResponseType.SUCCESS) == 0) {
      System.out.println("..::||");
      System.out.println("..::|| SUCCESS : " + container + " has been invoked");
      System.out.println("..::||");
    }

    System.out.println("..::|| response.getMsg(): " + response.getMsg() + " ||::..");
    System.out.println("..::|| response.getResult(): " + response.getResult() + " ||::..");

    assertTrue(true);
  }

}
